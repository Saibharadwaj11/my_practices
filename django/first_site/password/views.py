from django.shortcuts import render

def password_check(password1,password2):
     
    Specialchar =' @#*/_;!@[]$#^&.,?'
    val = 0
     
    if len(password1) < 8:
        return('length should be at least 8')
        val = 1
         
    if not any(char.isdigit() for char in password1):
        return('Password should have at least one numeral')
        val = 2
         
    if not any(char.isupper() for char in password1):
        return('Password should have at least one uppercase letter')
        val = 3
         
    if not any(char.islower() for char in password1):
        return('Password should have at least one lowercase letter')
        val = 4
         
    if not any(char in Specialchar for char in password1):
        return('Password should have at least one of the symbols @#*/_;!@[]')
        val = 5
    if password1 != password2:
        return('Password dont Match')
        val = 6
    if not val:
        return 'strong password'

def validate_password(request):
    context = {}
    if request.method ==  "POST":
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        context = {'password1' : password1,'password2' : password2}
        context['reason'] =  password_check(password1, password2)
    return render(request, 'password/checker.html',context)

