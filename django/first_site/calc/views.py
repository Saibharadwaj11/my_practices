from django.shortcuts import render
import random
# Create your views here.

def quiz(request):
    op1 = random.randint(0, 10)
    op2 = random.randint(0, 10)
    operator = random.choice(['+', '-', '*'])
    context = {'op1': op1, 'op2': op2, 'operator': operator}

    if request.method == 'POST':
        op1 = int(request.POST.get('op1', 0))
        op2 = int(request.POST.get('op2', 0))
        operator = request.POST.get('operator', '+')
        correct_solution = {'+': op1 + op2, '-': op1 - op2, '*': op1 * op2}[operator]
        submitted_solution = int(request.POST.get('solution', 0))
        success = correct_solution == submitted_solution
        if (success):
            request.session['streak'] = request.session.get('streak', 0) + 1
        else:
            request.session['streak'] = success
        context['success'] = success
    
    return render(request, 'calc/quiz.html', context)
