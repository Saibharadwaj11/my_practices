from django.shortcuts import render, redirect
from .models import Notice

def create_notice(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        author = request.POST.get('author')
        content = request.POST.get('content')
        Notice.objects.create(title=title, author=author, content=content)
        return redirect('notice_list')
    return render(request, 'noticeboard/create_notice.html')

def notice_list(request):
    notices = Notice.objects.all().order_by('-created_at')[:6]
    return render(request, 'noticeboard/notice_list.html', {'notices': notices})