from django.urls import path
from .views import create_notice, notice_list

urlpatterns = [
    path('notice/create/', create_notice, name='create_notice'),
    path('notice/', notice_list, name='notice_list'),
]