from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name

class food(models.Model):
    img = models.URLField(max_length=256, null=True)
    name = models.CharField(max_length=256)
    price = models.DecimalField(decimal_places=2, max_digits=10)
    

    def _str_(self):
        return f'{self.img}- {self.name} - {self.cost}'

class Bill(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(food, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, default=0)