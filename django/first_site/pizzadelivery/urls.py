from django.urls import path
from . import views


urlpatterns = [
    path('menu', views.delivery_app, name='menu'),
    path('login', views.login_, name='login'),
    path('logout', views.logout_, name='logout'),
    path('order/<int:id>', views.order, name='order'),
    path('increment/<int:id>', views.increment,name='increment'),
    path('decrement/<int:id>', views.decrement,name='decrement')
]