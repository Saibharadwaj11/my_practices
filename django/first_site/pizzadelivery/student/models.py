from django.db import models

# Create your models here.
class student(models.Model):
    rollno = models.IntegerField()
    marks = models.PositiveIntegerField()

    def __str__(self):
        return str(self.rollno)

class Class(models.Model):
    student = models.ForeignKey(student, on_delete=models.CASCADE)
    room_no = models.IntegerField()


