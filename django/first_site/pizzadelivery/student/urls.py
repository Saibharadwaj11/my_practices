from django.urls import path
from . import views
urlpatterns = [
    path("add_marks", views.add_marks, name = "add_marks"),
    path("records", views.records, name = "records"),
    path("validate",views.validate_marks, name = "validate"),
    path('find_rank', views.find_rank, name='find_rank')
]