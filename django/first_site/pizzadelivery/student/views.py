from django.shortcuts import render, redirect
from .models import student

# Create your views here. 
def add_marks(request):
    return render(request, "student/add_marks.html")

def validate_marks(request):
    if request.method ==  "POST":
        roll_no = int(request.POST["rollno"])
        marks = int(request.POST.get("marks"))
        college = student(rollno = roll_no, marks = marks )
        college.save()
        return redirect('records')

def records(request):
    students = list(student.objects.all())
    students.sort(key = lambda x : x.marks, reverse = True)
    return render(request, "student/record.html", {"students" : students} )

def find_rank(request):
    context = {}
    if request.method == 'POST':
        rollno = request.POST.get('rollno')
        students = list(student.objects.all())
        students.sort(key = lambda x : x.marks, reverse = True)
        student_ = student.objects.get(rollno = rollno)
        rank = students.index(student_) + 1
        context['rank'] = rank
        context['rollno'] = rollno
    return render(request, 'student/search.html', context)