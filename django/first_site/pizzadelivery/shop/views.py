from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required, permission_required
from .models import Product, Category, CartItem

# Create your views here.

def product_list(request):
    products = Product.objects.all()
    return render(request, 'shop/home.html', {'products': products})

@permission_required('shop.add_cartitem', login_url='shop:login')
def add_to_cart(request, product_id):
    product_ = Product.objects.get(pk=product_id)
    try:
        cartitem = CartItem.objects.get(product=product_, user=request.user)
    except CartItem.DoesNotExist:
        CartItem.objects.create(
            product=product_,
            user=request.user)
    cartitem, created = CartItem.objects.get_or_create(product=product_, user=request.user)
    cartitem.quantity += 1
    cartitem.save()
    return redirect('shop:cart')

@permission_required('shop.view_cartitem',login_url='shop:login')
def view_cart(request):
    cartitems = CartItem.objects.filter(user=request.user)
    return render(request, 'shop/cart.html', {'cartitems': cartitems})