from django.shortcuts import render
from django.shortcuts import render, redirect
from .models import food, Bill
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required

# Create your views here. 
@login_required(login_url= 'login') 
def delivery_app(request):
    menu = food.objects.all()
    return render(request, 'pizzadelivery/food.html', {'menu': menu})

def login_(request):
    if request.method == 'GET':
        return render(request, 'pizzadelivery/login.html')
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        entered_password = request.POST.get('password', '')
        user = authenticate(username=username_, password=entered_password)
        print(user)
        if user is not None:
            login(request, user)
        return redirect('delivery_app')

def logout_(request):
    logout(request)
    return redirect('login')

@login_required(login_url= 'login')
def order(request, id):
    items = food.objects.get(id = id)
    try:
        bill = Bill.objects.get(order = items,user=request.user)
    except Bill.DoesNotExist:
        Bill.objects.create(order = items,user=request.user)
    bill.quantity = bill.quantity + 1
    bill.subtotal = bill.quantity * bill.order.price
    bill.save()
    billing = Bill.objects.filter(user = request.user)
    total = sum([item.subtotal for item in billing])
    gst = (total * 3)//100
    grandtotal = total + gst
    return render(request,'pizzadelivery/bill.html', {'billing': billing, 'total' : total, 'grandtotal' : grandtotal})

def increment(request,id):
    quantity = Bill.objects.get(id = id)
    quantity.quantity += 1
    quantity.subtotal = quantity.quantity * quantity.order.price
    quantity.save()
    billing = Bill.objects.filter(user = request.user)
    total = sum([item.subtotal for item in billing])
    gst = (total * 3)//100
    grandtotal = total + gst
    return render(request,'pizzadelivery/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})

def decrement(request,id):
    quantity = Bill.objects.get(id = id)
    if quantity.quantity > 1:
        quantity.quantity -= 1
        quantity.subtotal = quantity.quantity * quantity.order.price
        quantity.save()
        billing = Bill.objects.filter(user = request.user)
        total = sum([item.subtotal for item in billing])
        gst = (total * 3)//100
        grandtotal = total + gst
        return render(request,'pizzadelivery/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})
    
    elif quantity.quantity == 1 : 
        quantity.delete()
        billing = Bill.objects.filter(user = request.user)
        total = sum([item.subtotal for item in billing])
        gst = (total * 3)//100
        grandtotal = total + gst
        return render(request,'pizzadelivery/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})