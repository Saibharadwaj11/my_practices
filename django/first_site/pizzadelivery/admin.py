from django.contrib import admin
from .models import food, Bill

# Register your models here.
admin.site.register([food, Bill])
