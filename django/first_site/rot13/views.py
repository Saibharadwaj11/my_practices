from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
    
def rot13(text):
    result = ""
    for char in text:
        if char.isalpha():
            shift = 13
            if char.isupper():
                result += chr((ord(char) - 65 + shift) % 26 + 65)
            else:
                result += chr((ord(char) - 97 + shift) % 26 + 97)
        else:
            result += char
    return result

def process_rot13_form(request):
    context = {}
    if request.method == 'POST':
        original_text =  request.POST.get('text', '')
        converted_text = rot13(request.POST.get('text',''))
        context['converted_text']  = converted_text
        context['original_text'] = original_text
    return render(request, 'rot13/temp.html', context) 





