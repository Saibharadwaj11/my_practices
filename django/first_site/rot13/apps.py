from django.apps import AppConfig


class Rot13Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rot13'
