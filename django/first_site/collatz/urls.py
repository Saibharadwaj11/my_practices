from django.urls import path
from . import views
urlpatterns = [
    path('series/form',views.collatz_sequence_form, name = 'collatz'),
    path('hello', views.call_collatz, name = 'template')
]