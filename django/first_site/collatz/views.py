from django.shortcuts import render
from django.http import HttpResponse

# Create your views here

def collatz(number):
    lst=[]
    lst.append(number)
    while(number!=1):
        if(number%2==0):
            number=number//2
            lst.append(number)
        else:
            number=number*3+1
            lst.append(number)
    return lst

def collatz_sequence_form(request):
    if request.method == 'POST':
        limit = int(request.POST.get('limit'))
        series = collatz(limit)
        return HttpResponse(f'''
        <!DOCTYPE html>
        <html>
        <body>
        <h1>collatz_Sequence</h1>
        <p>{series}</p>
        </body>
        </html>
        ''')

def call_collatz(request):
    #use the request.GET dictionary
    #name = request.GET.get('name')
    # num_codes = int(request.GET.get('collatz_sequence_form', 100)) - 1
    context = {'collatz_sequence': collatz_sequence_form}
    return render(request, 'collatz/template.html', context) 